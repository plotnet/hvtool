#!/bin/bash


function update(){
    scrpath=`readlink -f $0`
    cd `dirname $scrpath`
    killall hvagent 
    git pull
    mkdir -p logs
    bin/hvagent > logs/hvagent.log 2>&1 &
}

function space_clean(){
    docker container prune -f
    docker image prune -f
    docker volume prune -f
}

space_clean
update

echo "`date '+%Y/%m/%d %T'` finish daily work!"

