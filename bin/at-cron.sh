#!/bin/bash

sleep $2

scrpath=$HOME/autorun/$1
logpath=$HOME/hvtool/logs
mkdir -p $logpath
date >> $logpath/$1.log
find $scrpath/* -exec \{\} \; >> $logpath/$1.log

