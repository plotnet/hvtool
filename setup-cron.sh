#!/bin/bash
hvtool=`readlink -f $0`
hvpath=`dirname $hvtool`
dailypath=$HOME/autorun/daily
bootpath=$HOME/autorun/reboot

mkdir -p $dailypath
mkdir -p $bootpath
cd $bootpath
ln -sf $hvpath/start-agent.sh .

cd $dailypath
ln -sf $hvpath/start-agent.sh .
ln -sf $hvpath/daily.sh .

hostname=`hostname`
delay=${hostname#*-}
while [ $delay -ge 60 ]; do
	delay=`expr $delay - 60`
done

crontab <<EOF
@reboot $hvpath/bin/at-cron.sh reboot $delay
0 0 * * * $hvpath/bin/at-cron.sh daily $delay

EOF

