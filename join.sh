#!/bin/bash

host1=${HOSTNAME:0:1}
prepare_hdd(){
    if [ $host1 == "h" ]; then
	sudo mkdir -p /opt/plots
        for plotdir in  b c d e ; do
            if [ ! -d /opt/plots/${plotdir} ]; then
                sudo mkdir /opt/plots/${plotdir}
            fi
        done

        grep "^# added by harvester installer-d$"  /etc/fstab >/dev/null
        if (( $? == 1 )); then
            sudo patch /etc/fstab patch/fstab
	        sudo mount /opt/plots/b
	        sudo mount /opt/plots/c
	        sudo mount /opt/plots/d
	        sudo mount /opt/plots/e
        fi
    fi
}

which docker >/dev/null
if [ "$?" -ne "0" ]; then
    mkdir logs
    sudo apt update
    sudo apt install -y docker.io
    sudo usermod -a -G docker $USER
    echo "Please login again to activate docker, and then run $0 again"
else
    prepare_hdd
    docker swarm leave -f
    docker container prune -f
    docker volume prune -f
    docker swarm join --token SWMTKN-1-23mxoo745xwg78h65rmtcgpyurhvlyv4373abd9jbmxbd6bf16-1df0pntitzey3efd7jvxevtg7 1.14.62.141:2377
    ./setup-cron.sh
fi


