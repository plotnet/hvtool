#!/bin/bash
scrpath=`readlink -f $0`
cd `dirname $scrpath`
killall -q hvagent 
git pull
mkdir -p logs
bin/hvagent > logs/hvagent.log 2>&1 &
